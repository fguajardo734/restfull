<%@page import="DAO.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="DAO.Producto"%>
<%@page import="modelo.DAOProducto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Busqueda por ID</title>
    </head>
    <body>
        
            <%
            HttpSession session2 = request.getSession();
            Usuario user = (Usuario)session2.getAttribute("usuario");
            if(user == null){
            request.getRequestDispatcher("Error.jsp").forward(request, response);
            }else{
            out.println("<h1>Estimado "+user.getUsuario()+" aqui se muestra la busqueda solicitada</h1>");
            }
            %>
        
        <table border="1" style="text-align:center;">
          <tr><th>Identificador</th><th>Nombre</th><th>Precio</th><th>Codigo_Producto</th></tr>
        <%
       DAOProducto daoproducto = new DAOProducto();
       List<Producto> datos = new ArrayList();
       String buscar = (String)session.getAttribute("busca"); 
       datos = daoproducto.consulta1(buscar);
       
       for(Producto p: datos){
      %>     
       <tr><td><%= p.getProd_codigo() %></td><td><%= p.getProd_nombre() %></td><td><%= p.getProd_precio() %></td><td><%= p.getProd_cod() %></td></tr>
      
      <% 
       }
       %> 
            </table>
    </body>
</html>
