package DAO;

public class Usuario {
    private String usuario;
    private String clave;
    
    public Usuario(String user, String pass){
    this.usuario = user;
    this.clave = pass;
    }
    
    public String getUsuario(){
        return usuario;
    }
    
    public void setUsuario(String usuario){
        this.usuario = usuario;
    }
    
        public String getClave(){
        return clave;
    }
    
    public void setClave(String clave){
        this.clave = clave;
    }
}
