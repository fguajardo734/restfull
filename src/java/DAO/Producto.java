/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author fguaj
 */
public class Producto {
    private int prod_codigo;
    private String prod_nombre;
    private int prod_precio;
    private int prod_cod;
    
        public Producto(int prod_codigo, String prod_nombre, int prod_precio, int prod_cod){
    this.prod_codigo = prod_codigo;
    this.prod_nombre = prod_nombre;
    this.prod_precio = prod_precio;
    this.prod_cod = prod_cod;
    }

    public Producto() {
    }

    
    public int getProd_codigo(){
        return prod_codigo;
    }
    
    public void setProd_codigo(int prod_codigo){
        this.prod_codigo = prod_codigo;
    }
    
        public String getProd_nombre(){
        return prod_nombre;
    }
    
    public void setProd_nombre(String prod_nombre){
        this.prod_nombre = prod_nombre;
    }
    
        public int getProd_precio(){
        return prod_precio;
    }
    
    public void setProd_precio(int prod_precio){
        this.prod_precio = prod_precio;
    }
    
        public int getProd_cod(){
        return prod_cod;
    }
    
    public void setProd_cod(int prod_cod){
        this.prod_cod = prod_cod;
    }
}
