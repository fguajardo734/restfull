/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import DAO.Categoria;
import DAO.Operaciones;
import DAO.Producto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


    public class DAOProducto implements Operaciones {
Dbconexion db = new Dbconexion();
    
    @Override
    public String insertar(Producto producto) {
        Connection con;
        PreparedStatement ps=null;
        
	try {
                Class.forName(db.getDriver());
                con = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPass());
		String query="insert into examen.tbl_producto(prod_codigo,prod_nombre,prod_precio,prod_cod) values (null,?,?,?)";
		ps=con.prepareStatement(query);
		ps.setString(1, producto.getProd_nombre());
		ps.setInt(2, producto.getProd_precio());
		ps.setInt(3, producto.getProd_cod());
		System.out.println(ps);
		ps.executeUpdate();
	} catch (Exception e) {
		System.out.println(e);
	}
        return "exito";
    }

    @Override
    public List<Producto>listar() {
        
     List<Producto> datos = new ArrayList<>();
     List<Categoria> data = new ArrayList<>();
     Connection con;
     PreparedStatement pst;
     ResultSet rs;     
     String query2 = "select * from examen.tbl_producto prod INNER JOIN examen.tbl_categoria cat ON prod.prod_cod = cat.cat_cod_categoria;";
     
        try {
            Class.forName(db.getDriver());
            con = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPass());
            pst = con.prepareStatement(query2);
            rs = pst.executeQuery();
            
            while(rs.next())
            {
            datos.add(new Producto(rs.getInt("prod_codigo"), rs.getString("prod_nombre"), rs.getInt("prod_precio"), rs.getInt("prod_cod")));
            }
            
            con.close();
                                   
        } catch (ClassNotFoundException | SQLException e) {
        }
        return datos;
    }
    
   @Override
    public List<Producto> consulta1(String busca) {
     List<Producto> datos = new ArrayList<>();
     Connection con;
     PreparedStatement pst;
     ResultSet rs;
     String SQLone = "select * from examen.tbl_producto where prod_codigo =";
     String SQLtwo = busca;
     String SQLF = SQLone+SQLtwo;
        try {
            Class.forName(db.getDriver());
            con = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPass());
            pst = con.prepareStatement(SQLF);
            rs = pst.executeQuery();
            while(rs.next())
            {
            datos.add(new Producto(rs.getInt("prod_codigo"), rs.getString("prod_nombre"), rs.getInt("prod_precio"), rs.getInt("prod_cod")));
            }
            con.close();          
        } catch (ClassNotFoundException | SQLException e) {
        }
        return datos;
    }

   @Override
    public List<Producto> consulta2(String busc2) {
     List<Producto> datos = new ArrayList<>();
     Connection con;
     PreparedStatement pst;
     ResultSet rs;       
        try {
            Class.forName(db.getDriver());
            con = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPass());
            pst = con.prepareStatement("select * from examen.tbl_producto where prod_nombre like '%"+busc2+"%'");
            rs = pst.executeQuery();
            while(rs.next())
            {
            datos.add(new Producto(rs.getInt("prod_codigo"), rs.getString("prod_nombre"), rs.getInt("prod_precio"), rs.getInt("prod_cod")));
            }
            con.close();                     
        } catch (ClassNotFoundException | SQLException e) {
        }
        return datos;
    }
    }