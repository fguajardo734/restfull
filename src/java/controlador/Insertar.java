package controlador;
import DAO.Producto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.Usuario;
import javax.servlet.RequestDispatcher;
import modelo.DAOProducto;

@WebServlet(name = "Insertar", urlPatterns = {"/Insertar"})
public class Insertar extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String nombre = request.getParameter("nombre");
        String precio = request.getParameter("precio");
        String cat = request.getParameter("cat");
        String respuesta = "";
        
        if(nombre.equalsIgnoreCase("")||precio.equalsIgnoreCase("")||cat.equalsIgnoreCase("")){
            request.getRequestDispatcher("Error.jsp").forward(request, response);
        }else{
            DAOProducto dao = new DAOProducto();
        Producto p = new Producto();
        p.setProd_codigo(Integer.getInteger(request.getParameter(null)));
        p.setProd_nombre(request.getParameter("nombre"));
        p.setProd_precio(Integer.getInteger(request.getParameter("precio")));
        p.setProd_cod(Integer.getInteger(request.getParameter("cat")));
        respuesta = dao.insertar(p);
        
        
        RequestDispatcher rd = null;
        request.setAttribute("respuesta", respuesta);
        request.getRequestDispatcher("VistaProducto.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
