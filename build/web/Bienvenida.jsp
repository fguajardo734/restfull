<%@page import="java.util.List"%>
<%@page import="DAO.Producto"%>
<%@page import="modelo.DAOProducto"%>
<%@page import="DAO.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="Visi.js"></script>
        <script type="text/javascript" src="valida.js"></script>
        <title>Menu</title>
    </head>
    <body>
        <%
            HttpSession session2 = request.getSession();
            Usuario user = (Usuario)session2.getAttribute("usuario");
            if(user == null){
            request.getRequestDispatcher("Error.jsp").forward(request, response);
            }else{
            out.println("<h1>Bienvenido "+user.getUsuario()+"</h1>");
            }
            %>
            <table>
                <tr><th>Favor elija que requerira</th></tr>
                <tr><td>Insertar producto   </td><td><input type="radio" name="accion" id="radio1" onclick ="visible()"/></td></tr>
                <tr><td>Buscar producto     </td><td><input type="radio" name="accion" id="radio2" onclick ="visible()"/></td></tr>
                <tr><td>Mostar todo         </td><td><input type="radio" name="accion" id="radio3" onclick ="visible()"/></td></tr>
            </table>
            <br>
            <br>
            <div id="div1" style="display: none">
                <form method="post" action="Insertar" onsubmit="return validar()">
                    <b>Insertar Producto</b><br>
                <table border="1">
                <tr><td>Nombre Producto </td><td><input type="text"   name="nombre" id="nombre"/></td></tr>
                <tr><td>Precio Producto </td><td><input type="number" name="precio" id="precio"/></td></tr>
                <tr><td>Codigo Categoria</td><td><input type="number" name="cat"    id="cat"/></td></tr>
                </table>
                    <input type="submit" value="Ingresar valores">
                </form>
            </div>
            <div id="div2" style="display: none">
                
                   <b> Busqueda de Producto </b><br>
                <table border="1">
                    <tr><td>Por identificador  </td><td><input type="radio" name="accion1" id="radio4" onclick ="visible()"/></td><td>
                    <div id="div4" style="display: none">
                        <form method="post" action="Validador1" onsubmit="return validar()">
                        <input type="number" name="busc1" id="busc1">
                        <input type="submit" value="Procesar"> 
                        </form>
                        </div>
                    </td></tr>
                    <tr><td>Por nombre         </td><td><input type="radio" name="accion1" id="radio5" onclick ="visible()"/></td><td>
                    <div id="div5" style="display: none">
                        <form method="post" action="Validador2" onsubmit="return validar()">
                        <input type="text" name="busc2" id="busc2">
                        <input type="submit" value="Procesar"> 
                        </form>
                        </div>
                    </td></tr>
                </table>
               
            </div>
            <div id="div3" style="display: none">
                <table border="1" style="text-align:center;">
          <tr><th>Identificador</th><th>Nombre</th><th>Precio</th><th>Codigo_Producto</th></tr>
                 <%   
       DAOProducto daoproducto = new DAOProducto();
       List<Producto> datos = new ArrayList();      
       datos = daoproducto.listar();
       
       for(Producto p: datos){
      %>     
          <tr><td><%= p.getProd_codigo() %></td><td><%= p.getProd_nombre() %></td><td><%= p.getProd_precio() %></td><td><%= p.getProd_cod() %></td></tr>
      
      <% 
       }
       %> 
            </table>
            </div>
    </body>
</html>
